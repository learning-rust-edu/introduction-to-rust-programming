use std::fmt::{Debug, Display};

trait TimeTrait: Debug + Display {}

impl <T: Debug + Display> TimeTrait for T {}

fn main() {
    let mut v: Vec<Box<dyn Debug>> = Vec::new();
    v.push(Box::new(5));
    v.push(Box::new("Hello"));
    println!("v = {:?}", v);

    let mut v2: Vec<Box<dyn TimeTrait>> = Vec::new();
    v2.push(Box::new(42));
    v2.push(Box::new("Bye"));
    println!("v2 = {:?}", v2);
}
