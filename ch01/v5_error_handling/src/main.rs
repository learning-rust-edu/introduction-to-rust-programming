use std::fs::File;
use std::io::Read;
use std::env::args;

#[derive(Debug)]
pub enum MyErr {
    IOErr(std::io::Error),
}

impl From<std::io::Error> for MyErr {
    fn from(e: std::io::Error) -> Self {
        MyErr::IOErr(e)
    }
}

pub fn count_ws(filename: &str) -> Result<i32, MyErr> {
    let mut f = match File::open(filename) {
        Ok(v) => v,
        Err(e) => return Err(e.into()), //return Err(MyErr::IOErr(e)),
    };
    let mut s = String::new();
    if let Err(e) = f.read_to_string(&mut s) {
        // return Err(MyErr::IOErr(e));
        return Err(e.into());
    }
    let mut res = 0;
    for c in s.chars() {
        if c == 'w' {
            res += 1;
        }
    }

    Ok(res)
}

pub fn count_ws2(filename: &str) -> Result<i32, MyErr> {
    let mut s = String::new();
    File::open(filename)?.read_to_string(&mut s)?;
    Ok(s.chars().fold(0, |n, c| if c == 'w' { n + 1 } else { n }))
}

fn main() {
    for a in args().skip(1) {
        let w = count_ws(&a);
        println!("{} has {:?} 'w's", a, w);
        let w2 = count_ws2(&a);
        if let (Ok(a), Ok(b)) = (&w, &w2) {
            assert_eq!(a, b);
            println!("All OK!");
        }
    }
    println!("Hello, world!");
}
