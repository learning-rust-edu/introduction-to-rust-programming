use std::ops::{AddAssign, Add};

#[derive(PartialEq, Debug, Clone)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point<T> {
    pub fn new(x: T, y: T) -> Self {
        Point { x, y }
    }
}

impl<T: AddAssign + Clone> Point<T> {
    pub fn transpose(&mut self, p: &Self) {
        self.x += p.x.clone();
        self.y += p.y.clone();
    }
}

impl<T: Add<Output = T> + Clone> Add for Point<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point::new(
            self.x + rhs.x,
            self.y + rhs.y
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
        let p1 = Point::new(1, 7);
        let mut p2 = Point::new(2, 3);
        let p3 = p1 + p2.clone();
        assert_eq!(p3, Point::new(3, 10));

        p2.transpose(&Point::new(6, 6));

        let ps = Point::new("hello", "goodbye");
        //assert_eq!(p3, ps);
    }
}
