use std::env::args;

fn main() {
    for x in args().skip(1) {
        println!("hello {}", x);
    }
    for x in 0..10 {
        println!("that's {}", x);
    }
    for (k, v) in (10..20).enumerate() {
        println!("k = {}, v = {}", k, v);
    }
    for x in FibIter::new().take(15) {
        println!("fib = {}", x);
    }
    let mut i = FibIter::new().filter(|x| x % 2 == 0).take(10);
    while let Some(v) = i.next() {
        println!("other fib = {}", v);
    }
}

pub struct FibIter {
    a: i32,
    b: i32,
}

impl FibIter {
    pub fn new() -> Self {
        FibIter { a: 0, b: 1 }
    }
}

impl Iterator for FibIter {
    type Item = i32;

    fn next(&mut self) -> Option<Self::Item> {
        let c = self.a + self.b;
        self.a = self.b;
        self.b = c;
        Some(self.a)
    }
}
