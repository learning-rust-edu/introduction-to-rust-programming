fn main() {
    let x = 5;
    // int implements Copy
    drop(x);
    // x was copied
    println!("x = {}", x);

    let s = "hello".to_string();
    drop(s);
    // s was moved into the function "drop" and cannot be used after that
//    println!("s = {}", s);

    let mut x = 5;
    let mut y = 42;
    let pt = choose(&mut x, &mut y);
    *pt += 2;
    // x cannot be even read here because pt is borrowing it a mutable way
    // one mutable borrow or many immutable borrows are allowed
    println!("pt = {}", pt);
    x += 3;
    println!("x = {}, y = {}", x, y);

    // Pointers in structs
    let th = make_thing(&x, y);
    y += 2;
//    x += 2; // cannot do this
    println!("Thing = {:?}", th);
    println!("x = {}, y = {}", x, y);
}

pub fn choose<'a>(x: &'a mut i32, y: &'a mut i32) -> &'a mut i32 {
    if rand::random() {
        x
    } else {
        y
    }
}

pub fn make_thing(x: &i32, y: i32) -> SomeThing {
    SomeThing{
        pt: x,
        age: y,
    }
}

#[derive(Debug)]
pub struct SomeThing<'a> {
    pt: &'a i32,
    age: i32,
}

/*
pub fn get_y<'a>() -> &'a i32 {
    let mut y = 42;
    // local variables are destroyed when the function ends
    &y
}
*/